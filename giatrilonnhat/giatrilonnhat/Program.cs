﻿//cach 1 mang 1 cap
using System.Net.Http.Headers;

int[] arr = { 1, 2, 34, 5, 546, 768, 6, 45, 34, 23, 6666 };
int numberlager = 0;
foreach (int i in arr)
{
    if (i > numberlager)
    {
        numberlager = i;
    }
}
Console.WriteLine("number lager most {0}", numberlager);

//cach 2 mang 1 cap
int numberlager2 = 0;
int[] arr2 = { 1, 2, 34, 5, 546, 768, 6, 45, 34, 23, 6666 };
for (int i = 0; i < arr2.GetLength(0); i++)
{
    numberlager2 = Math.Max(arr2[i], numberlager2);
}

Console.WriteLine("number lager most {0}", numberlager);

//cach 3 mang 1 cap
int[] arr3 = { 1, 2, 34, 5, 546, 768, 6, 45, 34, 23, 6666 };
numberlager = 0;
for (int i = 0; i < arr3.GetLength(0); i++)
{
    if (arr3[i] > numberlager)
    {
        numberlager = arr3[i];
    }

}
Console.WriteLine("number lager most {0}", numberlager);


//cach 1 mang 2 cap
int[,] arr4 = { { 1, 2, 34, 5, 546, 768, 6, 45, 34, 23, 6666 }, { 1, 2, 34, 5, 546, 768, 6, 45, 34, 23, 6666 } };
numberlager = 0;

for (int i = 0; i < arr4.GetLength(0); i++)
{
    for (int j = 0; j < arr4.GetLength(1); j++)
    {
        if (arr4[i, j] > numberlager)
        {
            numberlager = arr4[i, j];
        }
    }
}

Console.WriteLine("number lager most {0}", numberlager);
//cach 2 mang 2 cap
int[,] arr5 = { { 1, 2, 34, 5, 546, 768, 6, 45, 34, 23, 6666 }, { 1, 2, 34, 5, 546, 768, 6, 45, 34, 23, 6666 } }; 
numberlager = 0;
foreach (var item in arr5)
{
    if (item > numberlager)
    {
        numberlager = item;
    }
}
Console.WriteLine("number lager most {0}", numberlager);

//cach 3 mang 2 cap 
int[,] arr6 = { { 1, 2, 34, 5, 546, 768, 6, 45, 34, 23, 6666 }, { 1, 2, 34, 5, 546, 768, 6, 45, 34, 23, 6666 } };
numberlager = 0;

for (int i = 0; i < arr6.GetLength(0); i++)
{
    for (int j = 0; j < arr6.GetLength(1); j++)
    {
        numberlager = Math.Max(arr6[i,j], numberlager); 
    }
}

Console.WriteLine("number lager most {0}", numberlager);
